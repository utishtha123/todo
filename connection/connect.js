const mongodb = require('mongodb');
const mongoose = require('mongoose');


module.exports = mongoose.connect('mongodb+srv://m001-student:m001-mongodb-basics@sandbox.i80gt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { useNewUrlParser: true , useUnifiedTopology: true})
    .then(() => { console.log('db connected') })
    .catch(() => {
        console.log("error while connecting to db")
    })
    
//mongodb+srv://m001-student:m001-mongodb-basics@sandbox.i80gt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
//mongodb://localhost:27017/Todo