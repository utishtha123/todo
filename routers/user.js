const User = require('../models/userModel');
const Todo = require('../models/todoModel');
//const { authorization } = require('../appHelper');
const { create_token } = require('../appHelper')

const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports.signup = async function (req, res) {
    try {
        let hashedPassword;
        if (!Object.keys(req.body.username) || !Object.keys(req.body.password) || !Object.keys(req.body.Image)) {
            res.status(400).json({ status: 0, message: 'please fill the missing fields' })

        }

        hashedPassword = await bcrypt.hash(req.body.password, saltRounds);

        let dataObj = {
            username: req.body.username,
            password: hashedPassword,
            Image: req.body.Image
        }

        await User.create(dataObj, (err, result) => {
            if (err) {
                console.log(err);
                res.status(400).json({ status: 0, message: err })
            }

            let user_data = {
                user_id: result.id,
                username: result.username,
            };
            let token = create_token(user_data);

            res.status(200).json({ status: 1, data: result , token:token });

        });

    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 0, message: error })
    }
}

module.exports.login = async function (req, res) {
    try {
        if (!Object.keys(req.body.username) || !Object.keys(req.body.password)) {
            res.status(400).json({ status: 0, message: 'please fill the missing fields' })
        }
        //console.log(req.body.username)

        await User.findOne({ username: req.body.username }, async (err, result) => {
            if (err) {
                console.log(err);
                res.status(400).json({ status: 0, message: "The user doesn't exists " })
            }

            let validatePassword = await bcrypt.compare(req.body.password, result.password);

            if (!validatePassword) {
                res.status(400).json({ status: 0, message: "The username or password incorrect " })
            } else {
                let user_data = {
                    user_id: result.id,
                    username: result.username
                };

                let token = create_token(user_data);

                res.status(200).json({ status: 1, message: "login successfull", token: token })
            }
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({ message: error })
    }
}