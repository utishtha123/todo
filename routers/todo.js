require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const Todo = require('../models/todoModel');
//const { authorization } = require('../appHelper');
const {getISTTime} = require('../appHelper');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports.createTodo = async (req, res)=> {
    try {

        if (!Object.keys(req.body.title) || !Object.keys(req.body.description)) {
            res.status(400).json({ status: 0, message: 'please fill the missing fields' })
        } else {
            let dataObj = {
                user_id: req.user_id,
                username: req.username,
                title: req.body.title,
                description: req.body.description,
               // valid_date: req.body.valid_date
            }

            await Todo.create(dataObj, (err, result) => {
                if (err) {
                    console.log(err)
                    res.status(400).json({ status: 0, err: err })
                }
                if(req.headers.timezone == '5.5'){
                    result.created_at = getISTTime(result.created_at);
                }
                res.status(200).json({ status: 1, data: result });
            });

        }

    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 0, message: error })
    }
}

module.exports.allTodo = async (req, res)=> {
    try {

        await Todo.find({ username: req.username, user_id: req.user_id }, (err, result) => {
            if (err) {
                console.log(err);
                res.status(400).json({ status: 0, err: err })
            }
            
            result.map(element =>{
                if(req.headers.timezone == '5.5'){
                    element.created_at = getISTTime(element.created_at);
                }
            })

            res.status(200).json({ status: 1, data: result });

        })

    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 0, message: error })
    }
}

module.exports.updateTodo = async (req, res)=> {
    try {
        if (!Object.keys(req.body.title) || !Object.keys(req.body.description)) {
            res.status(400).json({ status: 0, message: 'please fill the missing fields' })
        }

        let query = {
            title: req.body.title,
            description: req.body.description
        }

        await Todo.findOneAndUpdate({ _id: ObjectId(req.query.id) }, query, (err, result) => {
            if (err) {
                res.status(400).json({ status: 0, message: err })
            }
            if(req.headers.timezone == '5.5'){
                result.created_at = getISTTime(result.created_at);
            }
            res.status(200).json({ status: 1, data: result });
        })



    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 0, message: error })
    }
}

module.exports.deleteTodo = async (req, res)=> {
    try {
        
        await Todo.deleteOne({_id :ObjectId(req.query.id)}, (err, result) => {
            if (err) {
                res.status(400).json({ status: 0, message: err })
            }
            res.status(200).json({ status: 1, data: result });
        });


    } catch (error) {
        console.log(error);
        res.status(500).json({ status: 0, message: error })
    }
}

