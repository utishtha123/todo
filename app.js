//const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const mongodb = require('./connection/connect');
const Users = require('./controller/userController');
const Todo = require('./controller/todoController');

const app = express();
const server = require('http').Server(app);




app.use(express.static(__dirname));
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use('/api/user', Users);
app.use('/api/todo', Todo);

app.get('/test', (req,res)=>{
    res.send('test');
})

if (cluster.isPrimary) {
  console.log(`Primary ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {

  server.listen(8899);

  console.log(`Worker ${process.pid} started`);
}

