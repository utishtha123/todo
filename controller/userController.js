const express = require('express');
const Users = require('../routers/user');

const app = express();
const router = express.Router();


router.post('/signup', Users.signup);
router.post('/login', Users.login);

module.exports = router;