const express = require('express');
const Todo = require('../routers/todo');
const {authorization} = require('../appHelper');

const app = express();
const router = express.Router();

router.post('/createTodo', authorization , Todo.createTodo);
router.get('/allTodo', authorization, Todo.allTodo);
router.post('/updateTodo', authorization, Todo.updateTodo)
router.post('/deleteTodo', authorization, Todo.deleteTodo)

module.exports = router;