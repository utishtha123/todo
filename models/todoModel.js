const mongodb = require('../connection/connect');
const mongoose = require('mongoose');


const todoSchema = mongoose.Schema({
    user_id: String,
    username :String,
    title: String,
    description :String,
    valid_date: { type: Date, default: Date.now },
    created_at : { type: Date, default: Date.now }
});

const todo = mongoose.model('Todo', todoSchema)


module.exports = todo;

