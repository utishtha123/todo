const mongodb = require('../connection/connect');
const mongoose = require('mongoose');


const userSchema = mongoose.Schema({
    username : { type: String, unique: true },
    password : String,
    Image :{ type: String, default:''}
});

let User = mongoose.model('User', userSchema);

module.exports = User;

